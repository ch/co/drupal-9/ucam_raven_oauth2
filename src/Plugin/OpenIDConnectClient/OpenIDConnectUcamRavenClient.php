<?php

namespace Drupal\ucam_raven_oauth2\Plugin\OpenIDConnectClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\openid_connect\Plugin\OpenIDConnectClient\OpenIDConnectGoogleClient;

/**
 * UcamRaven OpenID Connect client.
 *
 * Implements OpenID Connect Client plugin for UcamRaven.
 *
 * @OpenIDConnectClient(
 *   id = "ucam_raven",
 *   label = @Translation("Raven")
 * )
 */
class OpenIDConnectUcamRavenClient extends OpenIDConnectGoogleClient {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['use_raven_signin_page'] = [
      '#title' => 'Use the normal Raven sign in page',
      '#description' => 'If set, <em>hd=cam.ac.uk</em> is added as a request parameter. See https://docs.raven.cam.ac.uk/en/latest/golden-rules/#use-hd-parameter-in-raven-oauth2-requests',
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['use_raven_signin_page'],
    ];

    $form['verify_hd_claim'] = [
      '#title' => 'Verify the hd claim is cam.ac.uk',
      '#description' => 'If set, only users verified as having <em>hd</em> set to <em>cam.ac.uk</em>. <strong>Warning:</strong> if this is unticked, it may be possible for users outside of the University of Cambridge to login to the website',
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['verify_hd_claim'],
    ];

    $form['verify_cam_email'] = [
      '#title' => 'Verify email address',
      '#description' => 'If set, only users with an email address ending <em>@cam.ac.uk</em> will be able to log in. <strong>Warning:</strong> if this is unticked, it may be possible for users outside of the University of Cambridge to login to the website',
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['verify_cam_email'],
    ];

    $google_url = 'https://console.developers.google.com/project/_/apiui/apis/library';
    $uis_url = 'https://docs.raven.cam.ac.uk/en/latest/apache-oauth2/';

    $form['description'] = [
      '#markup' => '<div class="description">' . $this->t('Set up your app in <a href="@google_url" target="_blank">Google API Console</a>.', ['@google_url' => $google_url]) . '</div>' . '<div class="description"> ' . $this->t('See the <a href="@uis_url">UIS documentation</a>', ['@uis_url' => $uis_url]) . '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints(): array {
    // Data from https://accounts.google.com/.well-known/openid-configuration
    $urls = [
      'authorization' => 'https://accounts.google.com/o/oauth2/v2/auth',
      'token' => 'https://oauth2.googleapis.com/token',
      'userinfo' => 'https://openidconnect.googleapis.com/v1/userinfo',
    ];

    if ($this->configuration['use_raven_signin_page']) {
      $auth_url = Url::fromUri($urls['authorization']);
      $auth_url->setOption('query', ['hd' => 'cam.ac.uk']);
      $urls['authorization'] = $auth_url->toString();
    }

    return $urls;
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveUserInfo(string $access_token = NULL): ?array {
    $userinfo = parent::retrieveUserInfo($access_token);
    if ($userinfo) {
      // For some reason Google returns the URI of the profile picture in a
      // weird format: "https:" appears twice in the beginning of the URI.
      // Using a regular expression matching for fixing it guarantees that
      // things won't break if Google changes the way the URI is returned.
      preg_match('/https:\/\/*.*/', $userinfo['picture'], $matches);
      $userinfo['picture'] = $matches[0];
    }

    return $userinfo;
  }

}
