# Description

This module provides a plugin for drupal/openid_connect to authorize users via
the Raven OAuth2 service. It also puts in place default configuration which
maintains the usually-expected Raven user experience.

# Installation instructions

If you have not done so, add the composer repo for this gitlab group:

- `composer config repositories.gitlab_7485 composer
  https://gitlab.developers.cam.ac.uk/api/v4/group/7485/-/packages/composer`

Note that this project also depends upon drupal/openid_connect. Then, require
and install the module as normal:

- `composer require ucam_drupal/ucam_raven_oauth2`
- `drush pm-install ucam_raven_oauth2`

At present, we just provide a "-dev" release so you will need to ensure
composer has been configured appropriately, e.g.

- `composer config prefer-stable`
- `composer config minimum-stability dev`

Upon installation, this module will enable the 'ucam_raven' openid_connect
which it provides. It will also set some core Drupal settings, as follows:

- Who can register accounts: Administrators only
- Require email verification when a visitor creates an account: off

as well as a setting for openid_connect:

-  Override registration settings: on

This combination will ensure that:

1. Accounts for Raven users will be automatically created as needed when a user
has authenticated
2. Non-Raven users cannot register accounts

If you want different behaviour, you can change the settings at
/admin/config/people/accounts and /admin/config/services/openid-connect to your
preferred values.

# Configuration

You will need to create a set of Google OAuth2 client credentials. See
https://docs.raven.cam.ac.uk/en/latest/creating-oauth2-client-credentials/ .

Then, provide the credentials: either for the 'Raven' plugin at
/admin/config/services/openid-connect , or via drush:

- `drush config-set openid_connect.client.ucam_raven settings.client_secret
  MyGoogleClientSecret -y`
- `drush config-set openid_connect.client.ucam_raven settings.client_id
  MyGoogleClientID.apps.googleusercontent.com -y`

The following configuration options should normally be left enabled unless you
have a defined reason to change them.

The most likely reason for wanting to disable any/all of these options will be
if you want to also permit non-Cambridge users to authenticate using this
plugin. However, you might be better off configuring one of the other
openid_connect plugins (e.g. Google or Generic) if you want to do this.

See also https://docs.raven.cam.ac.uk/en/latest/oauth2-claims/ and
https://docs.raven.cam.ac.uk/en/latest/golden-rules/ .

##  Use the normal Raven sign in page
This adds the hd=cam.ac.uk query parameter to the auth URL. This means that
users are automatically taken to the normal Raven login page. If set to off,

## Verify the hd claim is cam.ac.uk
If set, users will only be able to login if the hd claim is present and equal
to cam.ac.uk

## Verify email address
If set, users will only be able to login if the email claim ends with
@cam.ac.uk
